#pragma once
#include <eosio/eosio.hpp>
#include <eosio/asset.hpp>
#include <eosio/action.hpp>
#include <eosio/system.hpp>
using namespace eosio;
#include <vector>

using namespace std;

CONTRACT qmaintheory1 : public contract {
   
   public:
	using contract::contract;

	//   using qtokentheory1;

	qmaintheory1(name receiver, name code, datastream<const char*> ds): contract(receiver, code, ds), account(receiver, receiver.value), config(receiver, receiver.value), alienconf(receiver, receiver.value), aliens(receiver, receiver.value), rand(receiver, receiver.value) {};

	typedef uint16_t energy_type;
	typedef uint16_t durability_type;

	ACTION login( name owner );
	using login_action = action_wrapper<"login"_n, &qmaintheory1::login>;

	ACTION setconfig(	energy_type init_energy, energy_type init_max_energy, int32_t reward_noise_min, int32_t reward_noise_max, uint8_t min_fee, uint8_t	max_fee,uint64_t last_fee_updated,uint8_t fee);
	using setconfig_action = action_wrapper<"setconfig"_n, &qmaintheory1::setconfig>;

	ACTION rmconfig();
	using rmconfig_action = action_wrapper<"rmconfig"_n, &qmaintheory1::rmconfig>;

	ACTION newuser ( name owner );
	void _newuser(name owner);
	using newuser_action = action_wrapper<"newuser"_n, &qmaintheory1::newuser>;

	ACTION rmuser ( name owner );
	using rmuser_action = action_wrapper<"rmuser"_n, &qmaintheory1::rmuser>;

	ACTION withdraw (name owner, asset quantity);
	using withdraw_action = action_wrapper<"withdraw"_n, &qmaintheory1::withdraw>;

	ACTION recover (name owner, energy_type amount);
	using recover_action = action_wrapper<"recover"_n, &qmaintheory1::recover>;

	ACTION setenergy (name owner, energy_type amount);
	using setenergy_action = action_wrapper<"setenergy"_n, &qmaintheory1::setenergy>;

	ACTION setalienconf (string template_name, string img,string schema_name, uint8_t	rarity, durability_type	durability,string template_id, energy_type	energy_consumed, durability_type	durability_consumed, vector<asset> mints, vector<asset>	rewards, uint32_t charge_time);
	using setalienconf_action = action_wrapper<"setalienconf"_n, &qmaintheory1::setalienconf>;

	ACTION rmalienconf(string template_id);
	using rmalienconf_action = action_wrapper<"rmalienconf"_n, &qmaintheory1::rmalienconf>;

	ACTION mintassets(name owner, string template_id);
	using mintassets_action = action_wrapper<"mintassets"_n, &qmaintheory1::mintassets>;

	[[eosio::on_notify("atomicassets::transfer")]]
	void transfer_asset(name from, name to, vector<uint64_t> asset_ids, string memo);

	[[eosio::on_notify("qtokentheory::transfer")]]
	void deposit(const name& from,const name&    to,  asset   quantity, const string&  memo);

	ACTION addaliens(name owner, uint64_t asset, string template_id);
	using addaliens_action = action_wrapper<"addaliens"_n, &qmaintheory1::addaliens>;

	ACTION rmaliens(name asset);
	using rmaliens_action = action_wrapper<"rmaliens"_n, &qmaintheory1::rmaliens>;

	ACTION removeasset(name owner, uint64_t asset_id);
	using removeasset_action = action_wrapper<"removeasset"_n, &qmaintheory1::removeasset>;

	ACTION claim(name owner, uint64_t asset_id);
	using claim_action = action_wrapper<"claim"_n, &qmaintheory1::claim>;

	ACTION repair(const name &owner, uint64_t asset_id);
	using repair_action = action_wrapper<"repair"_n, &qmaintheory1::repair>;

	ACTION updatefeerng();
	using updatefeerng_action = action_wrapper<"updatefeerng"_n, &qmaintheory1::updatefeerng>;

	// ACTION testrand();
	// using testrand_action = action_wrapper<"testrand"_n, &qmaintheory1::testrand>;

	ACTION initrandom(name owner, uint64_t customer_id, uint64_t signing_value);
	ACTION getrandom(name owner);
	ACTION receiverand(uint64_t customer_id, const eosio::checksum256& random_value);
	using initrandom_action = action_wrapper<"initrandom"_n, &qmaintheory1::initrandom>;
	using getrandom_action = action_wrapper<"getrandom"_n, &qmaintheory1::getrandom>;
    using receiverand_action = action_wrapper<"receiverand"_n, & qmaintheory1::receiverand>;

private:
void sub_balance( const name& owner, const asset& value );
void add_balance( const name& owner, const asset& value );
void add_balance( const name& owner, const asset& value, const name& self );
void withdraw_rss( name owner, asset quantity, symbol sym);

/*

	ACCOUNT TABLE

*/
	struct [[eosio::table]]  account_table {
		name 			owner;
		energy_type 	energy;
		energy_type 	max_energy;
		vector<asset> 	balances;

		uint64_t primary_key()const { return owner.value; }
	};
	typedef eosio::multi_index< "accounts"_n, account_table > account_t;

	account_t account;

	/*

		CONFIG TABLE

	*/
	TABLE config_table {
		energy_type 	init_energy;
		energy_type 	init_max_energy;
		int32_t 		reward_noise_min;
		int32_t 		reward_noise_max;
		uint8_t		min_fee;
		uint8_t		max_fee;
		uint64_t		last_fee_updated;
		uint8_t		fee;

		uint64_t primary_key()const { return last_fee_updated; }
	};
	typedef eosio::multi_index< "config"_n, config_table > config_t;
	config_t config;

	/*

		ALIEN CONF

	*/

	TABLE alien_conf {
		string			template_name;
		string			img;
		string			schema_name;
		uint8_t			rarity;
		durability_type	durability;
		string			template_id;
		energy_type		energy_consumed;
		durability_type	durability_consumed;
		vector<asset>		mints;
		vector<asset>		rewards;
		uint32_t			charge_time;

		uint64_t primary_key()const { return stoull(template_id);}
	};
	typedef eosio::multi_index<"alienconf"_n, alien_conf > alienconf_t;
	alienconf_t alienconf;

	/*

		ALIENS

	*/

	TABLE alien {
		uint64_t 				asset_id;
		name 				owner;
		string 				template_id;
		durability_type 	durability_max;
		durability_type		durability;
		uint64_t			next_claim;
		uint64_t primary_key()const { return asset_id;};
		uint64_t getOwner()const {return owner.value;};
		uint64_t getTemplate()const {return stoull(template_id);};
	};
	typedef eosio::multi_index<"aliens"_n, alien, 
	indexed_by<"getowner"_n, const_mem_fun<alien, uint64_t,&alien::getOwner>>, 
	indexed_by<"gettemplate"_n, const_mem_fun<alien, uint64_t,&alien::getTemplate>>> alien_t;
	alien_t aliens;

	/*

		RAND

	*/
	
	TABLE rand_table {
		uint64_t	id;
		checksum256	number;

		uint64_t primary_key()const { return id;};
	};
	typedef eosio::multi_index<"rand"_n, rand_table> rand_t;
	rand_t rand;
	
	TABLE claim_table {
		uint64_t asset_id;
		vector<asset> last_claim;
		uint64_t primary_key()const { return asset_id;};
	};
	typedef eosio::multi_index<"lastclaim"_n, claim_table> last_claim_t;
	
};