#include <qmaintheory1.hpp>
ACTION qmaintheory1::login(name owner)
{
	require_auth(owner);
	print_f("Logged : %\n", owner);
	auto iterator = account.find(owner.value);
	if (iterator == account.end())
	{
		_newuser(owner);
	}
}

ACTION qmaintheory1::setconfig(energy_type init_energy, energy_type init_max_energy, int32_t reward_noise_min, int32_t reward_noise_max, uint8_t min_fee, uint8_t max_fee, uint64_t last_fee_updated, uint8_t fee)
{
	require_auth(get_self());

	auto conf = config.begin();

	if (conf == config.end())
	{
		config.emplace(_self, [&](auto &rec)
					   {
						   rec.init_energy = init_energy;
						   rec.init_max_energy = init_max_energy;
						   rec.reward_noise_min = reward_noise_min;
						   rec.reward_noise_max = reward_noise_max;
						   rec.min_fee = min_fee;
						   rec.max_fee = max_fee;
						   rec.last_fee_updated = last_fee_updated;
						   rec.fee = fee;
					   });
	}
	else
	{
		config.modify(conf, _self, [&](auto &rec)
					  {
						  rec.init_energy = init_energy;
						  rec.init_max_energy = init_max_energy;
						  rec.reward_noise_min = reward_noise_min;
						  rec.reward_noise_max = reward_noise_max;
						  rec.min_fee = min_fee;
						  rec.max_fee = max_fee;
						  rec.last_fee_updated = last_fee_updated;
						  rec.fee = fee;
					  });
	}

	print_f("Config updated");
}

ACTION qmaintheory1::rmconfig()
{
	check(config.begin() != config.end(), "no configuration file to delete");

	config.erase(config.begin());

	print_f("Config removed");
}

ACTION qmaintheory1::newuser(name owner)
{
	require_auth(get_self());

	check(account.find(owner.value) == account.end(), "user already exist.");

	account.emplace(_self, [&](auto &rec)
					{
						rec.owner = owner;
						rec.energy = config.begin()->init_energy;
						rec.max_energy = config.begin()->init_max_energy;
						rec.balances.push_back(asset(0, symbol("WOOD", 4)));
						rec.balances.push_back(asset(0, symbol("FOOD", 4)));
						rec.balances.push_back(asset(0, symbol("GOLD", 4)));
					});

	print_f("User created: %", owner);
}

void qmaintheory1::_newuser(name owner)
{

	check(account.find(owner.value) == account.end(), "user already exist.");

	account.emplace(_self, [&](auto &rec)
					{
						rec.owner = owner;
						rec.energy = config.begin()->init_energy;
						rec.max_energy = config.begin()->init_max_energy;
						rec.balances.push_back(asset(0, symbol("WOOD", 4)));
						rec.balances.push_back(asset(0, symbol("FOOD", 4)));
						rec.balances.push_back(asset(0, symbol("GOLD", 4)));
					});

	print_f("User created: %", owner);
}

ACTION qmaintheory1::rmuser(name owner)
{
	require_auth(get_self());

	auto iterator = account.find(owner.value);
	check(iterator != account.end(), "user doesn't exist.");

	account.erase(iterator);
	print_f("User deleted: %", owner);
}
ACTION qmaintheory1::withdraw(name owner, asset quantity)
{
	require_auth(owner);

	auto iterator = account.find(owner.value);
	check(iterator != account.end(), "user doesn't exist.");

	if (quantity.symbol.code().to_string() == "WOOD")
		withdraw_rss(owner, quantity, symbol("QTW", 4));
	else if (quantity.symbol.code().to_string() == "FOOD")
		withdraw_rss(owner, quantity, symbol("QTF", 4));
	else if (quantity.symbol.code().to_string() == "GOLD")
		withdraw_rss(owner, quantity, symbol("QTG", 4));
	else
		check(0, "invalid symbol");
}

ACTION qmaintheory1::recover(name owner, energy_type amount)
{
	require_auth(owner);

	const auto &player = account.get(owner.value, "no balance object found");

	check(player.energy != player.max_energy, "You are already full of energy !");
	check(player.energy + amount <= player.max_energy, "too much food provided");
	check(amount > 0, "provide a positive value");
	check(amount % 5 == 0, "energy only can recover 5 by 5");
	sub_balance(owner, asset(amount / 5 * 10000, symbol("FOOD", 4)));

	account.modify(player, owner, [&](auto &rec)
				   { rec.energy += amount; });

	print_f("Recovered energy: %\n", amount);
}

ACTION qmaintheory1::setenergy(name owner, energy_type amount)
{
	require_auth(get_self());
	const auto &player = account.get(owner.value, "no balance object found");
	account.modify(player, owner, [&](auto &rec)
				   { rec.energy = amount; });
}

ACTION qmaintheory1::setalienconf(string template_name, string img, string schema_name, uint8_t rarity, durability_type durability, string template_id, energy_type energy_consumed, durability_type durability_consumed, vector<asset> mints, vector<asset> rewards, uint32_t charge_time)
{
	require_auth(get_self());

	const auto &iterator = alienconf.find(stoull(template_id));

	if (iterator == alienconf.end())
	{
		alienconf.emplace(_self, [&](auto &rec)
						  {
							  rec.template_name = template_name;
							  rec.img = img;
							  rec.schema_name = schema_name;
							  rec.rarity = rarity;
							  rec.durability = durability;
							  rec.template_id = template_id;
							  rec.energy_consumed = energy_consumed;
							  rec.durability_consumed = durability_consumed;
							  rec.mints = mints;
							  rec.rewards = rewards;
							  rec.charge_time = charge_time;
						  });
	}
	else
	{
		alienconf.modify(iterator, _self, [&](auto &rec)
						 {
							 rec.template_name = template_name;
							 rec.img = img;
							 rec.schema_name = schema_name;
							 rec.rarity = rarity;
							 rec.durability = durability;
							 rec.template_id = template_id;
							 rec.energy_consumed = energy_consumed;
							 rec.durability_consumed = durability_consumed;
							 rec.mints = mints;
							 rec.rewards = rewards;
							 rec.charge_time = charge_time;
						 });
	}
}

ACTION qmaintheory1::rmalienconf(string template_id)
{
	require_auth(get_self());

	const auto &iterator = alienconf.get(stoull(template_id), "no template found");

	alienconf.erase(iterator);
}

ACTION qmaintheory1::mintassets(name owner, string template_id)
{
	require_auth(owner);

	const auto &template_asset = alienconf.get(stoull(template_id), "no template found");

	for (int i = 0; i < template_asset.mints.size(); i++)
		sub_balance(owner, template_asset.mints[i]);

	action{
		permission_level{_self, "active"_n},
		"atomicassets"_n, "mintasset"_n,
		std::make_tuple(_self, "qassetstests"_n, "aliens"_n, atoi(template_id.c_str()), owner, vector<uint8_t>(0), vector<uint8_t>(0), vector<asset>(0))}
		.send();
}

void qmaintheory1::deposit(const name &from, const name &to, asset quantity, const string &memo)
{
	if (memo == "deposit")
	{
		if (quantity.symbol.code().to_string() == "QTW")
			quantity.symbol = symbol("WOOD", 4);
		else if (quantity.symbol.code().to_string() == "QTF")
			quantity.symbol = symbol("FOOD", 4);
		else if (quantity.symbol.code().to_string() == "QTG")
			quantity.symbol = symbol("GOLD", 4);
		else
			check(0, "invalid symbol");
		add_balance(from, quantity, _self);
	}
}

void qmaintheory1::withdraw_rss(name owner, asset quantity, symbol sym)
{
	sub_balance(owner, quantity);
	auto withdraw = quantity;
	auto total = quantity.amount;

	quantity.amount = (quantity.amount * (100 - config.begin()->fee)) / 100;

	asset fees(total - quantity.amount, sym);

	quantity.symbol = sym;
	action{
		permission_level{_self, "active"_n},
		"qtokentheory"_n, "transfer"_n,
		std::make_tuple(_self, "qfundstheory"_n, fees, string("withdraw fees"))}
		.send();
	quantity.symbol = sym;
	action{
		permission_level{_self, "active"_n},
		"qtokentheory"_n, "transfer"_n,
		std::make_tuple(_self, owner, quantity, string("withdraw"))}
		.send();
}

vector<string> split_str(string str, char sep)
{
	vector<string> key;

	int i = 0;
	key.push_back("");
	for (string::iterator it = str.begin(); it != str.end(); it++)
	{
		if (*it == sep)
		{
			key.push_back("");
			i++;
		}
		else
			key[i].push_back(*it);
	}
	return key;
}

void qmaintheory1::transfer_asset(name from, name to, vector<uint64_t> asset_ids, string memo)
{

	vector<string> memos = split_str(memo, ':');
	check(memos.size() == 2, "wrong memo format: action:template_id");
	const auto &template_asset = alienconf.get(stoull(memos[1]), "no template found");
	if (memos[0] == "stake")
	{
		aliens.emplace(_self, [&](auto &rec)
					   {
						   rec.asset_id = asset_ids.front();
						   rec.owner = from;
						   rec.template_id = memos[1];
						   rec.durability = template_asset.durability;
						   rec.durability_max = template_asset.durability;
						   rec.next_claim = 0;
					   });
	}
}

ACTION qmaintheory1::removeasset(name owner, uint64_t asset_id)
{
	require_auth(owner);
	const auto &alien = aliens.get(asset_id, "no asset it corresponding");
	string memo = "unstake:" + alien.template_id;
	check(alien.owner == owner, "you didn't own this alien");
	check(alien.next_claim <= current_time_point().sec_since_epoch(), "you can't remove item during claiming time");
	check(alien.durability == alien.durability_max, "you need to repair your item");

	aliens.erase(alien);
	vector<uint64_t> list;
	list.push_back(asset_id);
	last_claim_t last_claim(_self, owner.value);
	auto lc = last_claim.find(asset_id);
	if (lc != last_claim.end())
		last_claim.erase(lc);
	action{
		permission_level{_self, "active"_n},
		"atomicassets"_n, "transfer"_n,
		std::make_tuple(_self, owner, list, memo)}
		.send();
}

ACTION qmaintheory1::addaliens(name owner, uint64_t asset_id, string template_id)
{
	require_auth(get_self());
	const auto &template_asset = alienconf.get(stoull(template_id), "no template found");
	aliens.emplace(_self, [&](auto &rec)
				   {
					   rec.asset_id = asset_id;
					   rec.owner = owner;
					   rec.template_id = template_id;
					   rec.durability = template_asset.durability;
					   rec.durability_max = template_asset.durability;
					   rec.next_claim = 0;
				   });
}

ACTION qmaintheory1::rmaliens(name asset_id)
{
	require_auth(get_self());

	const auto &ass = aliens.get(asset_id.value, "no template found");
	aliens.erase(ass);
}

// Claim a tool and get the reward
ACTION qmaintheory1::claim(name owner, uint64_t asset_id)
{
	require_auth(owner);
	getrandom(owner);

	// Find the correct tool from the player
	const auto &tool = aliens.get(asset_id, "asset doesn't exist");

	// Check if it's the correct owner
	check(tool.owner.value == owner.value, "you don\'t own this tool");
	// Get claim timestamp from the tool
	const auto &info_tool = alienconf.get(stoull(tool.template_id), "tool provided doesn't exist");
	// Look for user's energy and timestamp avaibility
	const auto &user = account.get(owner.value, "user doesn't exist");
	check(user.energy - info_tool.energy_consumed >= 0, "you don\'t have enough energy to claim the tool");
	check(tool.next_claim <= current_time_point().sec_since_epoch(), "you can't claim this tool");
	check(tool.durability - info_tool.durability_consumed >= 0, "you don't have enough durability");

	// Modify tool and userproperties after claim
	aliens.modify(tool, _self, [&](auto &a)
				  {
					  a.next_claim = info_tool.charge_time*60 + current_time_point().sec_since_epoch();
					  a.durability -= info_tool.durability_consumed;
				  });
	account.modify(user, _self, [&](auto &a)
				   { a.energy -= info_tool.energy_consumed; });

	// Provide to owner the amount of resources
	vector<asset> rewards(info_tool.rewards.size());
	for (int i = 0; i < info_tool.rewards.size(); i++)
	{
		asset reward = info_tool.rewards[i];
		uint64_t max_value = static_cast<uint64_t>(config.begin()->reward_noise_max - config.begin()->reward_noise_min);
		auto &actual = rand.get(69696969696969, "invialid customer_id");
		auto byte_array = actual.number.extract_as_byte_array();

		uint64_t random_int = 0;
		for (int j = 0; j < 8; j++)
		{
			random_int <<= 8;
			random_int |= (uint64_t)byte_array[j];
		}

		int64_t bonus = random_int % max_value;
		bonus += static_cast<int64_t>(config.begin()->reward_noise_min);

		reward.amount = (reward.amount * (100 + bonus)) / 100;
		rewards[i] = reward;
		add_balance(owner, reward, _self);
	}
	last_claim_t last_claim(_self, owner.value);
	auto lc = last_claim.find(asset_id);
	if(lc == last_claim.end())
	{
		last_claim.emplace(owner, [&](auto& rec) {
			rec.asset_id = asset_id;
			rec.last_claim = rewards;
		});
	}
	else {
		last_claim.modify(lc, owner, [&](auto& rec) {
			rec.last_claim = rewards;
		});
	}
}

// Repair a tool if user have enough resources
ACTION qmaintheory1::repair(const name &owner, uint64_t asset_id) 
{
	require_auth(owner);

	// Get informations from table
	const auto &user = account.get(owner.value, "user doesn\'t exist");
	const auto &tool = aliens.get(asset_id, "tool doesn't exist");
	
	check(tool.durability != tool.durability_max, "your tool is already at its maximum durability");

	// Look if owner have enough resources to repair
	const auto &repair_price = abs(tool.durability_max - tool.durability) * 0.3 * 10000;
	check(user.balances[0].amount - repair_price >= 0.0, "you don't have enough resources to repair your tool");

	// Set durability's tool at maximum and remove resources from player
	aliens.modify(tool, _self, [&](auto &a) {
		a.durability = a.durability_max;
	});
	sub_balance(owner, asset(repair_price, symbol("GOLD", 4)));
}

void qmaintheory1::sub_balance(const name &owner, const asset &value)
{
	const auto &from = account.get(owner.value, "no balance object found");
	int i = 0;
	for (; from.balances[i].symbol != value.symbol; i++)
		;

	check(from.balances[i].amount >= value.amount, "overdrawn balance");

	auto payer = has_auth(owner) ? owner : _self;
	account.modify(from, payer, [&](auto &a)
				   { a.balances[i] -= value; });
}

void qmaintheory1::add_balance(const name &owner, const asset &value)
{
	const auto &to = account.get(owner.value, "no balance object found");

	int i = 0;
	for (; to.balances[i].symbol != value.symbol; i++)
		;

	auto payer = has_auth(owner) ? owner : _self;
	account.modify(to, payer, [&](auto &a)
				   { a.balances[i] += value; });
}

void qmaintheory1::add_balance(const name &owner, const asset &value, const name &self)
{
	const auto &to = account.get(owner.value, "no balance object found");

	int i = 0;
	for (; to.balances[i].symbol != value.symbol; i++)
		;

	auto payer = _self;
	account.modify(to, payer, [&](auto &a)
				   { a.balances[i] += value; });
}

ACTION qmaintheory1::updatefeerng()
{
	require_auth(_self);
	getrandom(_self);

	uint64_t max_value = static_cast<uint64_t>(config.begin()->max_fee - config.begin()->min_fee);
	auto &actual = rand.get(69696969696969, "invialid customer_id");
	auto byte_array = actual.number.extract_as_byte_array();

	uint64_t random_int = 0;
	for (int i = 0; i < 8; i++)
	{
		random_int <<= 8;
		random_int |= (uint64_t)byte_array[i];
	}

	uint64_t num1 = random_int % max_value;
	config.modify(config.begin(), _self, [&](auto &rec)
				  { rec.fee = rec.min_fee + static_cast<uint8_t>(num1); });
}

ACTION qmaintheory1::initrandom(name owner, uint64_t customer_id, uint64_t signing_value)
{
	require_auth(_self);
	action(
		{get_self(), "active"_n},
		"orng.wax"_n,
		"requestrand"_n,
		std::tuple{customer_id, signing_value, get_self()})
		.send();
}

ACTION qmaintheory1::getrandom(name owner)
{
	auto &actual = rand.get(69696969696969, "invialid customer_id");
	uint64_t signing_value = static_cast<uint64_t>(current_time_point().sec_since_epoch()) + owner.value;
	action(
		{get_self(), "active"_n},
		"orng.wax"_n,
		"requestrand"_n,
		std::tuple{actual.id, signing_value, get_self()})
		.send();
}

ACTION qmaintheory1::receiverand(uint64_t customer_id, const checksum256 &random_value)
{

	require_auth("orng.wax"_n);

	auto itrCustomer = rand.find(customer_id);
	//if not, insert a new record
	if (itrCustomer == rand.end())
	{
		rand.emplace(_self, [&](auto &rec)
					 {
						 rec.id = customer_id;
						 rec.number = random_value;
					 });
	}
	else
		rand.modify(itrCustomer, _self, [&](auto &rec)
					{ rec.number = random_value; });
}

