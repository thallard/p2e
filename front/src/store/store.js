import { Api, JsonRpc, RpcError } from "eosjs";
import { JsSignatureProvider } from "eosjs/dist/eosjs-jssig";
import { createStore } from "vuex";

const store = createStore({
  state: {
    name: "",
    key: "",
    rpc: new JsonRpc("http://testnet.waxsweden.org", { fetch }),
    api: null,
    config: {},
    template: {
      308203: {
        name: "Common Alien",
        img: "QmQPSJc4Aks5yvhfePFKke8MorJ8jHfn5gS57jF4Y9rnoq",
        rarity: 1,
        durability: 50,
      },
      308206: {
        name: "Uncommon Alien",
        img: "QmQkWTHtGh61h8eQDyRgqzvsZdYCKNuR2bUWJyxjR7JqPh",
        rarity: 2,
        durability: 100,
      },
      308207: {
        name: "Rare Alien",
        img: "QmTBsozxYBGBwDxCeARomQKkWkSk7QksATXvkZ9Y1jgVMN",
        rarity: 3,
        durability: 200,
      },
      308208: {
        name: "Epic Alien",
        img: "QmSsqN7v4GDKgTb66pJLHxh1mV952fBbpqSH8VLVispGg7",
        rarity: 4,
        durability: 500,
      },
      308209: {
        name: "Legendary Alien",
        img: "QmNSzjsBCXran8xrMkwVEDb5Jn5tjk8jrmYowJqAdmPgnf",
        rarity: 5,
        durability: 750,
      },
      308210: {
        name: "God Alien",
        img: "QmTnMUkwtgRJWUeE3y4s4GRNMFACnbS1vU7bBPDee21Vta",
        rarity: 6,
        durability: 1250,
      },
    },
    airgrab: null,
    logout: {
      token_balances: [],
      rss_balances: [],
      energy: 0,
      max_energy: 0,
    },
    tools: [],
    chest: [],
    usable: [],
    user: {
      token_balances: [],
      rss_balances: [],
      energy: 0,
      max_energy: 0,
    }
  },
  actions: {
    generateApi: ({ state }) => {
      //Generate the signature of your account
      const signatureProvider = new JsSignatureProvider([state.key]);

      const rpc = state.rpc;

      //Generate api from signature and rpc
      state.api = new Api({
        rpc,
        signatureProvider,
        textDecoder: new TextDecoder(),
        textEncoder: new TextEncoder(),
      });
    },
    login: ({ state }, credentials) => {
      //Promise for await a return, resolve = success, reject = failed
      return new Promise((resolve, reject) => {
        const self = this;
        //Generate signature from credentials
        const signatureProvider = new JsSignatureProvider([credentials.key]);

        const rpc = state.rpc;

        //Generate api from signature
        const api = new Api({
          rpc,
          signatureProvider,
          textDecoder: new TextDecoder(),
          textEncoder: new TextEncoder(),
        });
        (async () => {
          try {
            //Try to link the action itself to check credentials
            await api.transact(
              {
                actions: [
                  {
                    account: "qmaintheory1",
                    name: "login",
                    authorization: [
                      {
                        actor: credentials.name,
                        permission: "active",
                      },
                    ],
                    data: {owner: credentials.name},
                  },
                ],
              },
              {
                blocksBehind: 3,
                expireSeconds: 30,
              }
            );
            //Set the stored api to the current
            state.api = api;
          } catch (e) {
            //Catch error if invalid credentials and return it
            console.dir(e);
            reject(false);
          } finally {
            //Validate credentials
            resolve(true);
          }
        })();
      });
    },
  },
});

export default store;