# P2E

## How to install

You need to have vuejs installed
```
npm install -g @vue/cli
```

then
```

cd p2e/front
npm install
npm run serve
```
The server is launched on [localhost:8080](http://localhost:8080)
## How to login

Go on [WaxTestnet](https://waxsweden.org/create-testnet-account/)

Create an account name then save somewhere all the credentials

Go on the server, when you are on the login page, put the name of your testnet freshly created, then your private active key (it's start with 5)
Check the keep logged in if you want then click on login
You're done

## Todo

- Put the store in another file to clear the code
- Add a store actions to generate transaction from object
